package com.devcamp.restapibookauthor.Controller;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.restapibookauthor.Controller.models.Author;
import com.devcamp.restapibookauthor.Controller.models.Book;

@RestController
@RequestMapping("/api")
@CrossOrigin
public class BookAuthorAPI {
  @GetMapping("/books")
  public ArrayList<Book> getListBook() {
    Author author1 = new Author("Nguyen Van A", "a@gamil.com", 'm');
    Author author2 = new Author("Pham Thi B", "b@gamil.com", 'f');
    Author author3 = new Author("Le Chi C", "c@gamil.com", 'm');
    System.out.println(author1.toString());
    System.out.println(author2.toString());
    System.out.println(author3.toString());
    Book book1 = new Book("Guita", author1, 100000, 1);
    Book book2 = new Book("Yoga", author2, 80000, 1);
    Book book3 = new Book("Math", author3, 120000, 1);
    System.out.println(book1.toString());
    System.out.println(book2.toString());
    System.out.println(book3.toString());
    ArrayList<Book> books = new ArrayList<Book>();
    books.add(book1);
    books.add(book2);
    books.add(book3);
    return books;
  }
}
